
//������⥪� ����� - �뢮��
#include <iostream>
//������⥪� ��� �㭪樨 setprecision(),-��।���� �筮��� �뢮�� ���
#include <iomanip>
#include <stdio.h>


// ������ୠ� ࠡ�� 2.

int main() {
int prog;
// ������ ����� ��������� �� 1 - 5.
std::cout << "\n\n\t������ ����� �ணࠬ�� (1, 2, 3, 4 ��� 5) � ������ enter\n\n";
std::cin>>prog;

if(prog == 1){
// �ணࠬ�� 1.
// �뢮� ������ � �ᯮ�짮������ ����䨪��஢ �ଠ�஢����.

std::cout <<std::setw(59)<<std::left<<"**********"<<std::right<<"�����"<<std::setw(59)<<std::right<<"**********\n\n\n\n";
std::cout <<std::setw(100)<<std::right<<"�������: ���. ��ࠧ������"<<std::endl;
std::cout <<std::setw(84)<<std::right<<"��㯯�: 310"<<std::endl;
std::cout <<std::setw(97)<<std::right<<"��㤥��: ���⨭ �����\n\n\n"<<std::endl;
std::cout <<std::setw(68)<<std::right<<"���� 2023�."<<std::endl;
std::cout <<"\n\n\t��� ��室� �� �ணࠬ�� ������ enter";

}
else if(prog == 2){
        // �ணࠬ�� 2.
        std::cout <<"\n\t���������� ������ ������� ����� ������\n";

        //�����᪨� ⨯ ������
        std::cout << "  bool:\t\t"<< sizeof(bool) << " bytes" << std::endl;
        //ᨬ����� ⨯� ������
        std::cout << "  char:\t\t" << sizeof(char) << " bytes" << std::endl;
        std::cout << "  wchar_t:\t" << sizeof(wchar_t) << " bytes" << std::endl;
        std::cout << "  char16_t:\t" << sizeof(char16_t) << " bytes" << std::endl;
        std::cout << "  char32_t:\t" << sizeof(char32_t) << " bytes" << std::endl;
        //楫쭮 - �᫥��� ⨯� ������
        std::cout << "  int:\t\t" << sizeof(int) << " bytes" << std::endl;
        std::cout << "  short:\t" << sizeof(short) << " bytes" << std::endl;
        std::cout << "  long:\t\t" << sizeof(long) << " bytes" << std::endl;
        //⨯ ������ � ������饩 �窮�
        std::cout << "  float:\t" << sizeof(float) << " bytes" << std::endl;
        std::cout << "  double:\t" << sizeof(double) << " bytes" << std::endl;

        /* ������� ����䨪����: signed (��६����� � ������⥫�묨 � ����⥫�묨 �᫠��)
        � unsigned (��६����� ⮫쪮 � ������⥫�묨 �᫠��).
           ����䨪���� ��⨬���樨 ࠧ���:  short � long (�� < 16� � �� < 32x ���)  */

        std::cout << "\n\t���������� ������ ������� ����� ������  � ��������������\n";

        std::cout << "  signed char       : " << sizeof(signed char) << " bytes" << std::endl;
        std::cout << "  unsigned char     : " << sizeof(unsigned char) << " bytes" << std::endl;
        std::cout << "  long double       : " << sizeof(long double) << " bytes" << std::endl;
        std::cout << "  long long         : " << sizeof(long long) << " bytes" << std::endl;
        std::cout << "  long int          : " << sizeof(long int) << " bytes" << std::endl;
        std::cout << "  short int         : " << sizeof(short int) << " bytes" << std::endl;
        std::cout << "  unsigned int      : " << sizeof(unsigned int) << " bytes" << std::endl;
        std::cout << "  signed int        : " << sizeof(signed int) << " bytes" << std::endl;
        std::cout << "  unsigned long int : " << sizeof(unsigned long int) << " bytes" << std::endl;
}
else if(prog == 3){
        // �ணࠬ�� 3.
        std::cout << "  �������� �।�� ��䬥��᪮�� �� ��� �ᥫ.\n";

        float a, b, c;
        std::cout << "  ����� ��ࢮ� �᫮ � ������ enter\n";
        std::cin >> a;
        std::cout << "  ����� ��஥ �᫮ � ������ enter\n";
        std::cin >> b;
        std::cout << "  ����� ���� �᫮ � ������ enter\n";
        std::cin >> c;
        float d;
        d = (a + b + c) / 3;
        std::cout << "  १����:  " << d;
}
else if(prog == 4){
        // �ணࠬ�� 4.
        std::cout << "\n  ���᫥��� ᪮��� ��אַ��������� ��������.\n";

        float distance, time, result;
        std::cout << "  ����� ����ﭨ� � ��. � ������ enter\n";
        std::cin >> distance;
        std::cout << "  ����� �६� � ��� � ������ enter\n";
        std::cin >> time;
        result = (distance / time);
        std::cout << "  १����:  " << result << "��/��\n";

        std::cout << "\n\n���᫥��� �᪮७��.\n";

        float velocity_1,velocity_2, time_1, result_1;
        std::cout << "  ����� ��砫��� ᪮���� � �/� � ������ enter\n";
        std::cin >> velocity_1;
        std::cout << "  ����� ������� ᪮���� � �/� � ������ enter\n";
        std::cin >> velocity_2;
        std::cout << "  ����� �६� � ᥪ㭤�� � ������ enter\n";
        std::cin >> time_1;
        result_1 = (velocity_2 - velocity_1) / time_1;
        std::cout << "  १����:  " << result_1 << "�/ᥪ ^2\n";

        std::cout << "\n\n���᫥��� ࠤ��� ��㣠.\n";

        float circumferenceLength, result_2;
        std::cout << "  ����� ����� ���㦭��� � �. � ������ enter\n";
        std::cin >> circumferenceLength;
        result_2 = circumferenceLength / 6.28;
        std::cout << "  १����:  " << result_2 << "�.\n";
}
else if(prog == 5){
        // �ணࠬ�� 5.

//�ᯮ��㥬 �� ⨯� ������ � ������饩 �窮�
//��ᢠ����� ��६���� �᫮�� ���祭��
double ex;
ex=34.50;
long double ex1(0.004000);
float ex2(123.005);
float ex3(146000);

//�뢮��� �� ���᮫� ���� � ��ᯮ���樠�쭮� �ଥ
std::cout << std::scientific << ex << std::endl;
std::cout << std::scientific << ex1 << std::endl;
std::cout << std::scientific << ex2 << std::endl;
std::cout << std::scientific << ex3 << std::endl;

/*
 * �������⢮ ������ ���(�� "�"): - 祬 �� �����, ⥬ ���� �������� �筮���
 * ex -  7
 * ex1 - 7
 * ex2 - 7
 * ex3 - 7
*/

}
return 0;
}

