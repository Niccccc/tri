
  #include<iostream>

  //TODO выбираем задание записью цифры в #define FUNC 1 (от 1 до 4)
  #define FUNC 4

  #if FUNC ==1
  // 1 задание - выражения решаем и проверяем свой результат
  int main(){
  std::cout << "\tрезультат выражений\n";
  std::cout << (true && true)||false;
  std::cout << std::endl;
  std::cout << (false && true)||true;
  std::cout << std::endl;

  /* следующее выражение взял полностью в скобки,
   * иначе вопреки логики программа выдавала
   * результат ноль  */
  std::cout << ((true && false)||false||true);
  std::cout << std::endl;
  std::cout << (5>6||4>3) && (7>8);
  std::cout << std::endl;
  std::cout << !(7>6||3>4);
  std::cout << std::endl;
  return 0;
  }

  #elif FUNC ==2
  //2 задание
  enum Const{
  // задаем константы в типе перечисления enum
  CONST_0 = - 2,
  CONST_1 = - 3,
  CONST_2 = - 6,
  CONST_3 = - 5,
  CONST_4,
  CONST_5 = - 3
  };

  int main(){
  // задаем 6ть логическиx констант,
  //предположим - сегменты индикатора
  const bool a = true;
  const bool b = false;
  const bool c = true;
  const bool d = false;
  const bool e = true;
  const bool f = false;
  // подставляем в выражения константы
  bool rezul1 = ((a && b) || (!c) && (d||e));
  bool rezul2 = ((a && b)||(c && d)||(!f));
  bool rezul3= ((a||b) && (c||d) && (e||f));

  // выводим результат выражений
  std::cout<< "\tвывод результатов значений\n";
  std::cout << rezul1 << std::endl;
  std::cout << rezul2 << std::endl;
  std::cout << rezul3 << std::endl;



  /* проверяем выражение на правду, подставив в нее
   * значения перечисления enum
   */
  std::cout<< "\tесли значения enum по решению соответствуют, то правда\n";
  if ((CONST_0 > CONST_1) && (CONST_2 < CONST_3) && (CONST_4!= CONST_5))
  {
  std::cout<< "\tПРАВДА";
  }
  return 0;
  }

  #elif FUNC ==3
  //3 задание -  проверяем законы де Моргана

  int main()
  {
  // присваиваем переменным a,b выражения де Моргана
  // и сравниваем их
  int a,b;
  a = !(true ||false);
  std::cout<< a << std::endl;
  b = (! true) && (!false);
  std::cout << b << std::endl;

  // если де Морган прав, то правда
  if(a == b){
  std::cout<<"правда\n";
  }
  else std::cout<<"ложь";

  // присваиваем переменным x,y выражения де Моргана
  // и сравниваем их
  int x,y;
  x = !(true && false);
  std::cout<< x << std::endl;
  y = (! true)||(!false);
  std::cout<< y << std::endl;

  // если де Морган прав, то правда
  if(x == y){
  std::cout<<"правда";
  }
  else std::cout<<"ложь";
  return 0;
}

  #elif FUNC ==4

  //4 задание
  using namespace::std;

  int main(){
  setlocale(LC_ALL,"RU");
  // просим пользователя ввести значения для переменных
  cout<<"  введите значения для переменных x, y, z, v - "
  "после каждого ввода нажать enter";
  cout<<endl;
  int x;
  cin>>x;
  cout<<endl;
  int y;
  cin>>y;
  cout<<endl;
  int z;
  cin>>z;
  cout<<endl;
  int v;
  cin>>v;
  cout<<endl;
  // вывод результатов выражений
  cout<<"  результат 1 выражения";
  cout<<endl;
  /* в данном выражении производится сложение паременных
   * и хотя мы вводим для переменной x свое значение,
   * это значение преобразуется согласно выражению  */
  x = y + z + v;
  cout<<"\t"<<x<<endl;
  cout<<"\t"<<y<<endl;
  cout<<"\t"<<z<<endl<<endl;
  cout<<"  результат 2 выражения";
  cout<<endl;
  /* в выражении ниже значение переменных приравнивается
   * к значению переменной, которая с правой стороны
   * (особенность чтения компилятором с права на лево) */
  x = y = z;
  cout<<"\t"<<x<<endl;
  cout<<"\t"<<y<<endl;
  cout<<"\t"<<z<<endl<<endl;
  cout<<"  результат 3 выражения";
  cout<<endl;
  /* в следующем выражении к переменной y прибавляется один,
   * пять складывается с переменной y, а переменная z
   * вычисляется как умножение значения z до этого выражения
   * на результат сложения   */
  z*= ++y + 5;
  cout<<"\t"<<x<<endl;
  cout<<"\t"<<y<<endl;
  cout<<"\t"<<z<<endl<<endl;

  cout<<"  результат 4 выражения";
  cout<<endl;
  /* следующее выражение логическое, и чтобы получить логический ноль
   * нужно иметь ноль для z или y и ноль для x или v  */
  int logicValue = x || (y && z) || v;
  cout<<"\t"<<logicValue;

  return 0;
  }

  #endif












    