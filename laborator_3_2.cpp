  #include <iostream>

  // TODO ���� �������

  /* � �⤥���� �㭪��� �ந������ ���樠������
   * ��६����� � ࠧ���묨 ⨯��� ������
  */

  // ⨯ long, ��� ���樠������
  auto fun1(long iValue,long value = 5) -> long
  {
  std::cout << "   १���� �뢮�� auto ���樠����樨 ⨯� long\n";
  std::cout << "\t" << iValue << "\t"<< value << std::endl;
  return iValue;
  }

  // ⨯ char, ��ﬠ� ���樠������
  auto fun2() -> char
  {
  std::cout << "   १���� �뢮�� ��אַ� ���樠����樨 ⨯� char\n";
  char cValue('a');
  std::cout << "\t"<< cValue << std::endl;
  return cValue;
  }

  // ⨯ bool, ��������� ���樠������
  auto fun3() -> bool
  {
  std::cout << "   १���� �뢮�� �������饩 ���樠����樨 ⨯� bool\n";
  bool bValue = true ;
  std::cout << "\t"<< bValue << std::endl;
  return bValue;
  }

  // ⨯ int, uniform ���樠������
  auto fun4() -> int
  {
  std::cout << "   १���� �뢮�� uniform ���樠����樨 ⨯� int\n";
  int lValue{5};
  std::cout << "\t"<< lValue << std::endl;
  return lValue;
  }

  int main()
  {
  // �뢮� ���᭥��� ��� ���짮��⥫� � ���᮫�
  std::cout <<"\n   3 �������\n\n";

  // �맮� �㭪権
  fun1(5);
  fun2();
  fun3();
  fun4();

  // �뢮� � ���᮫� ��������� ⨯�� ������
  std::cout <<"\n   �������� ⨯� int: �� - 2 147 438 648 �� 2 147 438 647 \n";
  std::cout <<"\n   �������� ⨯� long: �� - 2 147 438 648 �� 2 147 438 647 \n";
  std::cout <<"\n   �������� ⨯� bool: �� 0  �� 255 \n";
  std::cout <<"\n   �������� ⨯� char: �� - 128 �� 127 \n";
  std::cout <<"   ��� ��室� �� �ணࠬ�� ������ enter";
  return 0;
  }
